FROM mockserver/mockserver

ENV MOCKSERVER_INITIALIZATION_JSON_PATH=/initializer.json
ENV MOCKSERVER_WATCH_INITIALIZATION_JSON="true"

COPY initializer.json /
